#include "ros/ros.h"
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <cmath>
#include <iostream>
#include <sstream>
#include <vector>
#include <errno.h>

#include "opencv2/opencv.hpp"
#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"

#include "withrobot_camera.hpp" /* withrobot camera API */

int main(int argc, char **argv)
{
    ros::init(argc, argv, "ocam_usb_cam");
    ros::NodeHandle nh_;

    int camera_format = 3;
    ROS_ASSERT(nh_.hasParam("/ocam_usb_cam_node/camera_format"));
    nh_.getParam("/ocam_usb_cam_node/camera_format", camera_format);
    ROS_INFO_STREAM("Got param: camera_format = " << camera_format);

    std::string video_device = "/dev/video0";
    ROS_ASSERT(nh_.hasParam("/ocam_usb_cam_node/video_device"));
    nh_.getParam("/ocam_usb_cam_node/video_device", video_device);
    ROS_INFO_STREAM("Got param: video_device = " << video_device);


    ros::Publisher pub_img = nh_.advertise<sensor_msgs::Image>("ocam/image_raw", 1);

    //    ros::Publisher image_pub = nh_.advertise<sensor_msgs::Image>("autopilot/image_filtr", 1);

    /*
     * initialize oCam-1MGN
     *
     * [ supported image formats ]
     *
     * USB 3.0
	 *	[8] "8-bit Greyscale 1280 x 960 45 fps"
     * 	[7] "8-bit Greyscale 1280 x 720 60 fps"
	 * 	[6] "8-bit Greyscale 640 x 480 80 fps"
	 *	[5] "8-bit Greyscale 320 x 240 160 fps"
	 *
	 * USB 2.0
	 *	[4] "8-bit Greyscale 1280 x 960 22.5 fps"
     * 	[3] "8-bit Greyscale 1280 x 720 30 fps"
	 * 	[2] "8-bit Greyscale 640 x 480 80 fps"
	 *	[1] "8-bit Greyscale 320 x 240 160 fps"
	 *
     */

    Withrobot::Camera camera(video_device.c_str());
    int rosrate = 30;
    switch (camera_format)
    {
    case 8:
        camera.set_format(1280, 960, Withrobot::fourcc_to_pixformat('G', 'R', 'E', 'Y'), 1, 45);
        rosrate = 45;
        break;
    case 7:
        camera.set_format(1280, 720, Withrobot::fourcc_to_pixformat('G', 'R', 'E', 'Y'), 1, 60);
        rosrate = 60;
        break;
    case 6:
        camera.set_format(640, 480, Withrobot::fourcc_to_pixformat('G', 'R', 'E', 'Y'), 1, 80);
        rosrate = 80;
        break;
    case 5:
        camera.set_format(320, 240, Withrobot::fourcc_to_pixformat('G', 'R', 'E', 'Y'), 1, 160);
        rosrate = 160;
        break;
    case 4:
        camera.set_format(1280, 960, Withrobot::fourcc_to_pixformat('G', 'R', 'E', 'Y'), 2, 45);
        rosrate = 45;
        break;
    case 3:
        camera.set_format(1280, 720, Withrobot::fourcc_to_pixformat('G', 'R', 'E', 'Y'), 1, 30);
        rosrate = 30;
        break;
    case 2:
        camera.set_format(640, 480, Withrobot::fourcc_to_pixformat('G', 'R', 'E', 'Y'), 1, 80);
        rosrate = 80;
        break;
    case 1:
        camera.set_format(320, 240, Withrobot::fourcc_to_pixformat('G', 'R', 'E', 'Y'), 1, 160);
        rosrate = 160;
        break;
    default:
        camera.set_format(640, 480, Withrobot::fourcc_to_pixformat('G', 'R', 'E', 'Y'), 1, 80);
         rosrate = 80;
         break;
    }
    ros::Rate rate(rosrate);
    Withrobot::camera_format camFormat;
    camera.get_current_format(camFormat);

    /*
     * Print infomations
     */
    std::string camName = camera.get_dev_name();
    std::string camSerialNumber = camera.get_serial_number();

    printf("dev: %s, serial number: %s\n", camName.c_str(), camSerialNumber.c_str());
    printf("----------------- Current format informations -----------------\n");
    camFormat.print();
    printf("---------------------------------------------------------------\n");

    //camera.set_control("Exposure (Absolute)", 2);
    /*
     * Start streaming
     */
    if (!camera.start())
    {
        perror("Failed to start.");
        exit(0);
    }
    cv::Mat srcImg(cv::Size(camFormat.width, camFormat.height), CV_8UC1);
    cv::Mat outImg(cv::Size(camFormat.width, camFormat.height), CV_8UC1);

    ROS_INFO("ocam_usb_cam node start");
    int counter = 0;
    // cv:Point
    while (ros::ok())
    {
        /* Copy a single frame(image) from camera(oCam-1MGN). This is a blocking function. */
        int size = camera.get_frame(srcImg.data, camFormat.image_size, 1);
        /* If the error occured, restart the camera. */
        if (size == -1)
        {
            printf("error number: %d\n", errno);
            perror("Cannot get image from camera");
            camera.stop();
            camera.start();
            continue;
        }
        cv_bridge::CvImage img_bridge;
        sensor_msgs::Image img_msg; // >> message to be sent

        std_msgs::Header header;         // empty header
        header.seq = counter++;          // user defined counter
        header.stamp = ros::Time::now(); // time
        img_bridge = cv_bridge::CvImage(header, sensor_msgs::image_encodings::MONO8, srcImg);
        img_bridge.toImageMsg(img_msg); // from cv_bridge to sensor_msgs::Image
        pub_img.publish(img_msg);       // ros::Publisher pub_img = node.advertise<sensor_msgs::Image>("topic", queuesize);

        ros::spinOnce();
        rate.sleep();
    }
    return 0;
}
